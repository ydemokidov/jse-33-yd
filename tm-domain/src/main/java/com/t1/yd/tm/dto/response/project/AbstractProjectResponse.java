package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import com.t1.yd.tm.model.Project;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractProjectResponse extends AbstractResultResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

    public AbstractProjectResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
