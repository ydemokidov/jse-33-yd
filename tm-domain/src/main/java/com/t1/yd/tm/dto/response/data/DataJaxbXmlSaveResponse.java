package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataJaxbXmlSaveResponse extends AbstractResultResponse {

    public DataJaxbXmlSaveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataJaxbXmlSaveResponse() {

    }
}
