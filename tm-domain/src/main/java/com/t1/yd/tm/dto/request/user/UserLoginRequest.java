package com.t1.yd.tm.dto.request.user;

import com.t1.yd.tm.dto.request.AbstractRequest;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class UserLoginRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

}
