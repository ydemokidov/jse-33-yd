package com.t1.yd.tm.exception.system;

public class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! Insufficient privileges...");
    }

}
