package com.t1.yd.tm.exception.field;

public class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
