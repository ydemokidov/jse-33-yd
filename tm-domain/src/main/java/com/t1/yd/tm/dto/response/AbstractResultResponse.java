package com.t1.yd.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class AbstractResultResponse extends AbstractResponse {

    @Nullable
    protected Throwable throwable;
    private boolean success = true;

    public AbstractResultResponse(@Nullable final Throwable throwable) {
        this.throwable = throwable;
    }

    public AbstractResultResponse() {
        super();
    }
}
