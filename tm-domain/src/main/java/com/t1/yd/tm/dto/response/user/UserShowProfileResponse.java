package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserShowProfileResponse extends AbstractUserResponse {

    public UserShowProfileResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserShowProfileResponse(@Nullable User user) {
        super(user);
    }

    public UserShowProfileResponse() {
        super();
    }

}
