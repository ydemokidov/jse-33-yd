package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(Task task) {
        super(task);
    }

    public TaskRemoveByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
