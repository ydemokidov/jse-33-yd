package com.t1.yd.tm.dto.response.system;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class SystemInfoResponse extends AbstractResultResponse {

    private String systemInfo;

    public SystemInfoResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public SystemInfoResponse() {
    }

}
