package com.t1.yd.tm.component;

import com.t1.yd.tm.api.endpoint.*;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.endpoint.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.SessionRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.SystemUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.yd.tm.command";

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();


    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, propertyService, sessionService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(dataEndpoint);
    }

    private void initBackup() {
        backup.init();
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);

    }

    public void run(String[] args) {
        initDemoData();
        initLogger();
        initPID();
        initBackup();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void initDemoData() {
        userService.add(userService.create("admin", "admin", "admin@mail.ru", Role.ADMIN));
        userService.add(userService.create("user1", "user1", "user1@mail.ru"));
        userService.add(userService.create("user2", "user2", "user2@mail.ru"));

        String user1id = userService.findByLogin("user1").getId();
        String user2id = userService.findByLogin("user2").getId();

        projectRepository.add(user1id, new Project(user1id, "project1", "my first project"));
        projectRepository.add(user1id, new Project(user1id, "project2", "my 2nd project"));
        projectRepository.add(user1id, new Project(user1id, "project3", "my 3rd project"));
        projectRepository.add(user2id, new Project(user2id, "project4", "my 4th project"));
        projectRepository.add(user2id, new Project(user2id, "project5", "my 5th project"));

        taskRepository.add(user1id, new Task(user1id, "task1", "my 1st task"));
        taskRepository.add(user1id, new Task(user1id, "task2", "my 2nd task"));
        taskRepository.add(user2id, new Task(user2id, "task3", "my 3rd task"));
        taskRepository.add(user2id, new Task(user2id, "task4", "my 4th task"));

        projectTaskService.bindTaskToProject(user1id, taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(user1id, taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    private void exit() {
        System.exit(0);
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }
}