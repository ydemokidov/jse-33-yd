package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.ISessionRepository;
import com.t1.yd.tm.api.service.ISessionService;
import com.t1.yd.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
