package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.service.IAuthService;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.request.user.UserLogoutRequest;
import com.t1.yd.tm.dto.request.user.UserShowProfileRequest;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.dto.response.user.UserLogoutResponse;
import com.t1.yd.tm.dto.response.user.UserShowProfileResponse;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "com.t1.yd.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @Override
    public UserLoginResponse login(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLoginRequest request) {
        @NotNull String token = getAuthService().login(request.getLogin(), request.getPassword());
        @NotNull final UserLoginResponse response = new UserLoginResponse();
        response.setToken(token);

        return response;
    }

    @Override
    public UserLogoutResponse logout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull final UserLogoutRequest request) {
        check(request);
        return new UserLogoutResponse();
    }

    @Override
    public UserShowProfileResponse showProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserShowProfileRequest request) {
        check(request);
        return new UserShowProfileResponse();
    }

}
