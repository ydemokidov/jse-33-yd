package com.t1.yd.tm.api.service;

import com.t1.yd.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
