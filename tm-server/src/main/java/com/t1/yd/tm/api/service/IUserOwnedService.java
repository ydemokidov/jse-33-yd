package com.t1.yd.tm.api.service;

import com.t1.yd.tm.api.repository.IUserOwnedRepository;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.model.AbstractUserOwnedEntity;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IUserOwnedService<E extends AbstractUserOwnedEntity> extends IUserOwnedRepository<E> {

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Sort sort);

}
