package com.t1.yd.tm.command.task;

import com.t1.yd.tm.api.endpoint.ITaskEndpoint;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskEndpoint getTaskEndpointClient() {
        return getServiceLocator().getTaskEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@NotNull final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}